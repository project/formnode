
OVERVIEW

This module provide an interface to add node views as markup in forms.

INSTALLING FORMNODE

If you want to use form_store and form_collect modules to have a nicer admin
form, download and enable them.
If you want to benefit from the i18n translation module, then just install it.

WARNING

PostgreSQL has not been tested since last update. Need testing.

DOCUMENTATION

None, but will come.

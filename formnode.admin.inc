<?php

/**
 * @file
 * Append node to forms administration pages.
 */

/**
 * Get node label.
 */
function _formnode_get_label($nid) {
  $node = !is_object($nid) ? node_load($nid) : $nid;
  return $node ? check_plain($node->title) : t("Deleted node @nid", array('@nid' => $form_state['values']['nid']));
}

/**
 * Main settings configuration form.
 */
function formnode_admin_settings($form_state) {
  $form = array();

  $form['formnode']['formnode_enable_admin'] = array(
    '#type' => 'checkbox',
    '#title' => t("Enable append link on admin pages"),
    '#default_value' => variable_get('formnode_enable_admin', FALSE),
    '#description' => t("If checked, the <em>Append node</em> link will also appear when the user browse the admin section."),
  );

  return system_settings_form($form);
}

/**
 * Admin form list
 */
function formnode_list_page() {
  $result = pager_query("SELECT * FROM {formnode}", 16);
  $formnodes = array();
  while ($formnode = db_fetch_object ($result)) {
    $formnodes[] = $formnode;
  }
  return theme('formnode_list', $formnodes) . theme('pager') . drupal_get_form('formnode_admin_settings');
}

/**
 * Admin form list
 */
function formnode_edit_form($form_state, $form_id = NULL, $node = NULL) {
  $form = array();
  $formnode = NULL;

  if ($form_id && $node) {
    $formnode = formnode_instance_get($form_id, $node);
  }

  if ($formnode) {
    $form['fid'] = array(
      '#type' => 'value',
      '#value' => $formnode->fid,
    );
    $form['original_nid'] = array(
      '#type' => 'value',
      '#value' => $formnode->nid,
    );
    $form['fid_text'] = array(
      '#type' => 'textfield',
      '#title' => t("Form ID"),
      '#maxlength' => 128,
      '#description' => t("Type a form ID here."),
      '#default_value' => $formnode ? $formnode->fid : ($form_id ? $form_id : NULL),
      '#disabled' => TRUE,
    );
  }
  else {
    $form['fid'] = array(
      '#type' => 'textfield',
      '#title' => t("Form ID"),
      '#maxlength' => 128,
      '#description' => t("Type a form ID here."),
      '#default_value' => $formnode ? $formnode->fid : ($form_id ? $form_id : NULL),
      '#required' => TRUE,
      '#readonly' => isset($form_id),
    );
  }

  if (module_exists('node_selector')) {
    $form['nid'] = array(
      '#type' => 'node_selector',
      '#title' => t("Search for node"),
      '#default_value' => $formnode ? $formnode->nid : NULL,
      '#required' => TRUE,
    );
  }
  else {
    $form['nid'] = array(
      '#type' => 'textfield',
      '#title' => t("Search for node"),
      '#size' => 10,
      '#maxlength' => 10,
      '#description' => t("Type a node nid here."),
      '#default_value' => $formnode ? $formnode->nid : NULL,
      '#required' => TRUE,
    );
  }
  $form['weight'] = array(
    '#type' => 'select',
    '#title' => t("Weigth"),
    '#options' => array(-10 => -10, -9 => -9, -8 => -8, -7 => -7, -6 => -6, -5 => -5, -4 => -4, -3 => -3, -2 => -2, -1 => -1, 0 => 0, 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10),
    '#description' => t('Set this to the weight you want the node to be in the form.'),
    '#default_value' => $formnode ? $formnode->weight : 0,
  );

  $form['fieldset_info'] = array(
    '#type' => 'fieldset',
    '#title' => t("Embed in fieldset"),
    '#collapsible' => TRUE,
    '#collapsed' => $formnode ? (!$formnode->fieldset) : FALSE,
  );
  $form['fieldset_info']['fieldset'] = array(
    '#type' => 'checkbox',
    '#title' => t("Display node into a fieldset ?"),
    '#default_value' => (bool) $formnode ? $formnode->fieldset : FALSE,
  );
  $form['fieldset_info']['fieldset_collapsed'] = array(
    '#type' => 'checkbox',
    '#title' => t("Is the fieldset collapsed ?"),
    '#default_value' => (bool) $formnode ? $formnode->fieldset_collapsed : NULL,
    '#description' => t("This optio is effective only if fieldset option is checked. Non collapsed fieldset will generate a non collapsible fieldset."),
  ); 
  $form['fieldset_info']['fieldset_title'] = array(
    '#type' => 'textfield',
    '#title' => t("Fieldset title"),
    '#default_value' => $formnode ? $formnode->fieldset_title : NULL,
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => $formnode ? t("Update") : t('Save'),
    '#submit' => array('formnode_edit_form_submit'),
  );
//  if ($formnode) {
//    $form['buttons']['delete'] = array(
//      '#type' => 'submit',
//      '#value' => t("Delete"),
//      '#submit' => array('formnode_edit_form_delete_submit'),
//    );
//  }

  return $form;
}

/**
 * Edit form submit handler.
 */
function formnode_edit_form_submit($form, &$form_state) {
  // Delete older reference if nid has changed.
  if (isset($form_state['values']['original_nid']) && $form_state['values']['original_nid'] != $form_state['values']['nid']) {
    formnode_instance_delete($form_state['values']['fid'], $form_state['values']['original_nid']);
  }
  formnode_instance_save((object) $form_state['values']);
}

/**
 * Deletion confirm form.
 */
function formnode_delete_confirm_form($form_state, $form_id, $node = NULL) {
  $form = array();
  $form['fid'] = array(
    '#type' => 'value',
    '#value' => $form_id,
  );
  if ($node) {
    $form['nid'] = array(
      '#type' => 'value',
      '#value' => $node->nid,
    );
  }
  return confirm_form($form, t("Are you sure you want to unembed node <em>@title</em> into <em>@fid</em> form ?", array(
    '@title' => _formnode_get_label($node),
    '@fid' => $form_id,
  )), 'admin/build/formnode/list');
}

/**
 * Deletion submit handler.
 */
function formnode_delete_confirm_form_submit($form, &$form_state) {
  if (isset($form_state['values']['nid'])) {
    formnode_instance_delete($form_state['values']['fid'], $form_state['values']['nid']);
    drupal_set_message(t("Node <em>@title</em> has been unembeded from the form <em>@fid</em>", array(
      '@title' => _formnode_get_label($form_state['values']['nid']),
      '@fid' => $form_state['values']['fid'],
    )));
  }
  else {
    formnode_instance_delete($form_state['values']['fid']);
    drupal_set_message(t("All entries for <em>@fid</em> have been deleted", array('@fid' => $form_state['values']['fid'])));
  }
  $form_state['redirect'] = 'admin/build/formnode/list';
}

/**
 * Generate the admin form list of form nodes
 *
 * @param array &$formnodes
 *   array of formnodes
 *
 * @return string
 *   XHTML output
 */
function theme_formnode_list($formnodes) {
  $header = array(t('Form ID'), t('Node'), t('Weight'), t('Fieldset mode'), '', '');
  $rows = array();
  $options = array('query' => array('destination' => $_GET['q']));
  foreach ($formnodes as &$formnode) {
    if (($nid = intval($formnode->nid)) > 0) {
      $node = node_load($nid);
    }
    $fieldset_mode = t('No');
    if ($formnode->fieldset) {
      if ($formnode->fieldset_collapsed) {
        $fieldset_mode = t('Collapsed');
      }
      else {
        $fieldset_mode = t('Yes');
      }
    }
    $rows[] = array(
      $formnode->fid,
      check_plain($node->title),
      $formnode->weight,
      $fieldset_mode,
      l(t('edit'), 'admin/build/formnode/' . $formnode->fid . '/' . $formnode->nid . '/edit', $options),
      l(t('delete'), 'admin/build/formnode/' . $formnode->fid . '/' . $formnode->nid . '/delete', $options),
    );
  }  
  return theme('table', $header, $rows);
}
